import * as firebase from 'firebase';
import { config } from "./key";

firebase.initializeApp(config);

const databseRef = firebase.database().ref();

export const eventsRef = databseRef.child("events");
export const participantsRef = databseRef.child("participants");