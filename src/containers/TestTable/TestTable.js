import React, { Component } from 'react';
import ReactTable from 'react-table';
import { Row, Col } from 'react-materialize';

class TestTable extends Component {
  state = {
    data: [],
    pages: null,
    loading: false
  }

  render() {
    const columns = [{
      Header: 'Name',
      accessor: 'name' // String-based value accessors!
    }, {
      Header: 'Satuan',
      accessor: 'satuan',
      // Cell: props => <span className='number'>{props.value}</span> // Custom cell components!
    }, {
      // id: 'friendName', // Required because our accessor is not a string
      Header: 'Harga Pokok',
      accessor: 'primaryPrice'
    }, {
      Header: 'Code', // Custom header components!
      accessor: 'code'
    }
    ]
    return (
      <Row>
        <h2>TEST TABLE</h2>
        <Col m={12}>
          <ReactTable
            manual
            className="-striped -highlight"
            data={this.state.data}
            pages={this.state.pages}
            columns={columns}
            loading={this.state.loading}
            onFetchData={(state, instance) => {
              console.log('state page', state.page);
              this.setState({ loading: true });
              fetch(`http://localhost:8000/api/barang?page=${state.page}&limit=${state.pageSize}`)
                .then(res => res.json())
                .then(data => {
                  console.log(data);
                  this.setState({
                    data: data.docs,
                    pages: data.total,
                    loading: false
                  });
                })
            }}
            defaultPageSize={20}
          />
        </Col>
      </Row>
    )
  }
}

export default TestTable;