import React, { Component } from 'react';
import { Row, Col, Input, Button } from "react-materialize";
// import { participantsRef } from "../../config/firebase";
import logo from '../../assets/images/logo.png';

import Footer from '../../components/Footer/Footer';
import Aux from '../../hoc/Auxi';
import Spinner from '../../components/UI/Spinner/Spinner';
import { registerUrl } from "../../config/Api";

class RegisterEvent extends Component {

  state = {
    npm: '',
    name: '',
    email: '',
    hp: '',
    kelas: '',
    loading: false
  }

  onFormSubmitted = () => {
    const { npm, name, email, hp, kelas } = this.state;
    if (!!npm && !!name && !!email && !!hp && !!kelas) {
      if (this.validateEmail(email)) {
        if (window.confirm('Apakah kamu sudah yakin dengan semua data yang kamu masukan ?')) {
          let peserta = {
            npm: npm.toUpperCase(),
            name: name,
            email: email.toLowerCase(),
            hp: hp,
            kelas: kelas.toUpperCase(),
            paid: 'N'
          };
          this.setState({ loading: true });
          fetch(registerUrl, {
            method: 'POST',
            body: JSON.stringify(peserta),
            headers: new Headers({
              'Content-Type': 'application/json'
            })
          })
            .then(res => res.json())
            .then(data => {
              if (data.success) {
                this.setState({ loading: false, npm: '', name: '', email: '', hp: '', kelas: '' });
                this.props.history.push(`/events/${this.props.match.params.eventId}/register/success`);
              } else if (!data.success && data.msg) {
                this.setState({ loading: false });
                alert(data.msg);
              } else if (!data.success && !data.msg) {
                this.setState({ loading: false });
                alert('Oopss...! Ada Kesalahan saat menghubungkan ke Server!');
              }
            })
            .catch(err => {
              this.setState({ loading: false });
              console.log('err', err);
              alert('Oopss...! Ada Kesalahan saat menghubungkan ke Server!');
            });
          // participantsRef.orderByChild('npm').equalTo(this.state.npm).on('value', snapshot => {
          //   console.log('snapshot', snapshot.exists());
          //   if (snapshot.exists()) {
          //     alert('NPM tersebut telah terdaftar!');
          //   } else {
          //     participantsRef.push(peserta);
          //     this.setState({ npm: '', name: '', email: '', hp: '', kelas: '' });
          //     this.props.history.push(`/events/${this.props.match.params.eventId}/register/success`);
          //   }
          // });
        }
      } else {
        alert('Email nya Tidak Valid :)');
      }
    } else {
      alert('Pengisian Form Belum Valid, Lengkapi Pengisian!');
    }
  }

  validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  selectHandleChange = (e) => {
    this.setState({ kelas: e.target.value });
  }

  render() {
    const { loading } = this.state;
    let register = null;

    if (!loading) {
      register = (
        <Aux>
          <Row>
            <Col s={12}>
              <Input s={6} label="NPM / KTP" name="npm" value={this.state.npm} onChange={(e) => this.handleChange(e)} validate />
              <Input s={6} label="Nama Lengkap" name="name" value={this.state.name} onChange={(e) => this.handleChange(e)} validate />
              <Input s={6} type="email" label="Email" name="email" value={this.state.email} onChange={(e) => this.handleChange(e)} validate />
              <Input s={6} label="No. Hp" name="hp" value={this.state.hp} onChange={(e) => this.handleChange(e)} validate />
              <Input s={6} label="Kelas" name="kelas" value={this.state.kelas} placeholder="ex: S1/TI/06/B/M atau Umum" onChange={(e) => this.handleChange(e)} validate />
              {/* <Input s={6} type='select' name="kelas" label="Asal Jurusan / Umum" value={this.state.kelas} onChange={(e) => this.selectHandleChange(e)}>
                <option value='Teknik Informatika'>Teknik Informatika</option>
                <option value='Sistem Informasi'>Sistem Informasi</option>
                <option value='Manajemen Informatika'>Manajemen Informatika</option>
                <option value='Komputerisasi Akuntansi'>Komputerisasi Akuntansi</option>
                <option value='Umum'>Umum</option>
              </Input> */}
            </Col>
          </Row>
          <Row>
            <Col s={12}>
              <Button type="button" onClick={this.onFormSubmitted}>Register</Button>
            </Col>
          </Row>
        </Aux>
      );
    } else {
      register = <Spinner />
    }
    return (
      <Aux>
        <div className="container">
          <div className="center">
            <img src={logo} width="200px" height="200px" alt="PPSR logo" />
          </div>
          <h4 className="center blue-text">Pendaftaran Peserta</h4>
          <small className="red-text">*) Untuk Umum NPM bisa diganti dengan no KTP</small>
          {register}
        </div>
        <Footer />
      </Aux>
    );
  }
}

export default RegisterEvent;