import React, { Component } from 'react';

import Aux from '../../hoc/Auxi';
import LandingPage from '../../components/LandingPage/LandingPage';
import Footer from '../../components/Footer/Footer';
import Spinner from '../../components/UI/Spinner/Spinner';


class Home extends Component {
  state = {
    loading: false
  }

  componentWillMount () {
    this.setState({ loading: true });
  }

  componentDidMount () {
    // this code for see loading effect.
    setTimeout(() => {
      this.setState({ loading: false });          
    }, 500);
    // this.setState({ loading: false });  
  }

  render() {
    const { loading } = this.state;
    let home = <Spinner />;
    if(!loading) {
      home = <LandingPage />;
    }
    return (
      <Aux>
        { home }
        <Footer />
      </Aux>
    );
  }
}

export default Home;