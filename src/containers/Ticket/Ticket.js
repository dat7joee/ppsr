import React, { Component } from 'react';
import { Row, Input, Button } from "react-materialize";
import logo from '../../assets/images/logo.png';
import Aux from '../../hoc/Auxi';
import { qrUrl } from "../../config/Api";
import QRPage from '../../components/QR/QR';
import { Link } from "react-router-dom";
import Spinner from '../../components/UI/Spinner/Spinner';

class Ticket extends Component {
  state = {
    npm: '',
    submitted: false,
    loading: false,
    registered: false,
    paid: false,
    done: false,
    data: {}
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value, submitted: false, done: false });
  }

  resetChange = () => {
    this.setState({
      loading: false,
      registered: false,
      paid: false,
      done: true,
      submitted: false,
      data: {}
    });
  }

  getQRHandler = () => {
    const { npm } = this.state;
    if (!npm) {
      alert('Isi Dulu NPM Nya ya :)');
    } else {
      this.setState({ submitted: true, done: false });
      let data = {
        npm: this.state.npm.toUpperCase()
      };

      fetch(qrUrl, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: new Headers({
          'Content-Type': 'application/json'
        })
      })
        .then(res => res.json())
        .then(data => {
          console.log('data', data);
          if (data.registered && data.paid) {
            let dataFetched = { ...data.data };
            this.setState({ loading: false, registered: true, paid: true, data: dataFetched, done: true, submitted: false });
          } else if (data.registered && !data.paid) {
            this.setState({ loading: false, registered: true, paid: false, done: true, submitted: false });
          } else if (!data.registered && !data.paid) {
            this.resetChange();
          }
        })
        .catch(err => {
          console.log('err', err);
          this.resetChange();
          alert('Ooppss...!! Telah Terjadi Kesalahan Saat Menghubungi Server!');
        });
    }
  }

  render() {
    let { registered, paid, data, submitted, done } = this.state;
    let qr = null;

    if (submitted && !done) {
      qr = <Spinner />;
    }

    if (!submitted && done) {
      if (registered && paid) {
        let dummy = {
          npm: data.npm,
          name: data.name,
          kelas: data.kelas,
          event: this.props.match ? this.props.match.params.id : 'PPSR'
        };
        let dataPeserta = JSON.stringify(dummy);
        qr = <QRPage value={dataPeserta} data={dummy} />;
      } else if (registered && !paid) {
        qr = <h4 className="center">Maaf, Kamu Belum Melakukan Pembayaran :(</h4>;
      } else if (!registered && !paid) {
        qr = <h4 className="center">Maaf, Kamu Belum Terdaftar Sebagai Peserta :( <Link to="/events" className="blue-text">Yuk Daftar!</Link></h4>;
      }
    }
    return (
      <Aux>
        <div className="container">
          <h2 className="header center blue-text">{this.props.match ? this.props.match.params.id : 'PPSR'} QR Ticket</h2>
          <div className="center">
            <img src={logo} width="200px" height="200px" alt="PPSR logo" />
          </div>
          <Row className="center">
            <Input s={12} m={12} l={12} label="NPM / KTP" name="npm" value={this.state.npm} onChange={(e) => this.handleChange(e)} />
            <Button s={12} m={6} l={6} onClick={this.getQRHandler}>GET TICKET</Button>
          </Row>
          {qr}
        </div>
      </Aux>
    );
  }
}

export default Ticket;