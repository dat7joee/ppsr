import React, { Component } from 'react';

import Aux from '../../hoc/Auxi';
import Events from '../../components/Events/Events';
import Footer from '../../components/Footer/Footer';

class EventPage extends Component {
  render() {
    return (
      <Aux>
        <Events />
        <Footer />
      </Aux>
    );
  }
}

export default EventPage;