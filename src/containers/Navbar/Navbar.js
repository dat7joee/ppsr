import React from 'react';
import { Navbar } from "react-materialize";
import { NavLink } from 'react-router-dom';


const NavigationBar = (props) => (
  <Navbar brand="PPSR" className="light-blue lighten-1" right>
    <li><NavLink to="/">HOME</NavLink></li>
    <li><NavLink to="/events">EVENTS</NavLink></li>
  </Navbar>
);

export default NavigationBar;