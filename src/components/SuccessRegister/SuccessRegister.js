import React from 'react';
import { Row } from "react-materialize";
import logo from '../../assets/images/logo.png';
import Aux from '../../hoc/Auxi';
import Footer from "../Footer/Footer";

const successRegister = () => {
  return (
    <Aux>
      <div className="container">
        <Row>
          <div className="center">
            <img src={logo} width="400px" height="400px" alt="PPSR logo" />
          </div>
          <h4 className="header center">TERIMA KASIH SUDAH BERPARTISPASI DALAM EVENT INI</h4>
          <h5 className="center">Silahkan Lakukan Proses Transfer untuk Iuran Pembayaran senilai <strong>Rp. 50.000</strong> Ke Rekening Berikut ini :</h5>
          <h5 className="center"><strong>6050362513 BANK BCA a/n DZIKRA RIFAT AKMAL</strong></h5>
          <h5 className="center"><strong>1560-0132-5251-7 BANK MANDIRI a/n INES</strong></h5>
          <hr />
          <p className="center">
            Setelah melakukan Proses Transfer, Silahkan Konfirmasi Via <strong>WhatsApp</strong> ke <strong>DESSY 0857-1846-7381</strong>
          </p>
        </Row>
      </div>
      <Footer />
    </Aux>
  );
};

export default successRegister;