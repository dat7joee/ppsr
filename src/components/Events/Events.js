import React, { Component } from 'react';
import { Row } from 'react-materialize';
import EventCard from './EventCard/EventCard';
import { eventsRef } from "../../config/firebase";

import Spinner from '../UI/Spinner/Spinner';

class Events extends Component {

  state = {
    events: [],
    loading: false
  }

  componentWillMount() {
    this.setState({ loading: true });
  }

  componentDidMount() {
    this.getEventsData();
  }

  getEventsData = () => {
    eventsRef.on('value', snapshot => {
      let data = [];
      snapshot.forEach(snap => {
        var temp = { ...snap.val() };
        if (!temp.completed) {
          data.push(snap.val());
        }
      });
      this.setState({ events: data, loading: false });
    }, err => {
      alert('Oopss..! Gagal Menghubungkan ke Server!');
    });
  }

  registerHandler = () => {
    console.log('you clicking this');
    // let eventData = {
    //   title: 'PPSR 2018',
    //   tagline: '-',
    //   image: 'https://cdn-images-1.medium.com/max/900/1*EntHChgUyirgbZ9A3zTxkA.png',
    //   date: 'To be Announced',
    //   location: 'To be Declared',
    //   completed: false
    // }
    // eventsRef.push().set(eventData);
  }

  render() {
    const { loading } = this.state;
    let eventRender = null;
    console.log('state', this.state.events);

    let eventList = this.state.events.map(eventData => (
      <EventCard
        key={eventData.eventId}
        eventId={eventData.eventId}
        title={eventData.title}
        tagline={eventData.tagline}
        date={eventData.date}
        image={eventData.image}
        location={eventData.location}
        completed={eventData.completed} />
    ));

    eventRender = <Spinner />

    if (!loading) {
      eventRender = eventList;
    }

    if (!loading && eventList.length <= 0) {
      eventRender = <h4 className="header center"><strong>Belum Ada Event Untuk Saat Ini :)</strong></h4>;
    }

    return (
      <div className="container">
        <Row>
          <h2 className="header center blue-text">Our Events</h2>
          {eventRender}
        </Row>
      </div>
    );
  }
}

export default Events;