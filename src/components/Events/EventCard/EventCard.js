import React from 'react';
import { Row, Col, Card, CardTitle } from 'react-materialize';
import { Link } from 'react-router-dom';

const eventCard = (props) => {
  let buttonList = [];
  let link = `/events/${props.eventId}/register`;
  let ticket = `/events/${props.eventId}/ticket`;
  if (!props.completed) {
    buttonList = [
      <Link to={link} className="blue-text">Daftar Sekarang</Link>,
      <Link to={ticket} className="blue-text">Ambil Tiket</Link>
    ];
  }
  return (
    <Row>
      <Col m={12} s={12}>
        <Card
          className="large"
          header={<CardTitle image={props.image}>{props.title}</CardTitle>}
          actions={buttonList}>
          <h5><strong>{props.tagline}</strong></h5>
          <p>Tanggal: {props.date}</p>
          <p>Lokasi: {props.location}</p>
        </Card>
      </Col>
    </Row>
  );
};

export default eventCard;