import React from 'react';
import QRCode from 'qrcode.react';
import { Row } from "react-materialize";
import Aux from '../../hoc/Auxi';

const qrPage = (props) => (
  <Aux>
    <Row className="center">
      <QRCode value={props.value} size={300} />
      <p>NPM: {props.data.npm}</p>
      <p>Nama: {props.data.name}</p>
      <p>Kelas: {props.data.kelas}</p>
    </Row>
  </Aux>
);

export default qrPage;