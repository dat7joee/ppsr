import React from 'react';
import { Row, Col } from 'react-materialize';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/logo.png';

const landingPage = (props) => (
  <div className="section no-pad-bot">
    <div className="container">
      <br /><br />
      <Row>
        <Col s={12} m={12}>
          <h1 className="header center blue-text">PPSR STMIK Bani Saleh</h1>
          <div className="center">
            <img src={logo} width="400px" height="400px" alt="PPSR logo" />
          </div>
          <Row className="center">
            <Col s={12}><h5 className="header light">Selamat Datang di Portal PPSR STMIK BANI SALEH ANGKATAN 2015</h5></Col>
          </Row>
          <Row className="center">
            <Link to="/events" className="btn-large waves-effect waves-light blue">CHECK EVENTS</Link>
          </Row>
        </Col>
      </Row>
    </div>
  </div>
);

export default landingPage;