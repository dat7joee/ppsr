import React from 'react';
import { Row, Col } from 'react-materialize';

const footer = (props) => (
  <footer className="page-footer blue">
    <div className="container">
      <Row>
        <Col l={6} s={12}>
          <h5 className="white-text">Tentang PPSR</h5>
          <p className="grey-text text-lighten-4">PPSR adalah Panitia Penyelenggara Santunan Ramadhan untuk Anak Yatim yang diselenggarakan Oleh Mahasiswa STMIK Bani Saleh Angkatan 2015 yang diadakan setiap Bulan Ramadhan.</p>
        </Col>
        <Col l={3} s={12}>
          <h5 className="white-text">Hubungi Kami</h5>
          <ul>
            <li className="white-text"><i className="material-icons" style={{ fontSize: '15px' }}>contact_phone</i> 0813-1440-3775 (Huda)</li>
            <li className="white-text"><i className="material-icons" style={{ fontSize: '15px' }}>contact_phone</i> 0812-8850-6693 (Dzikra)</li>
            <li className="white-text"><i className="material-icons" style={{ fontSize: '15px' }}>contact_phone</i> 0857-1846-7381 (Dessy)</li>
          </ul>
        </Col>
        <Col l={3} s={12}>
          <h5 className="white-text">Sosial Media</h5>
          <ul>
            <li className="white-text"><a href="http://fb.com/ppsr2k15" className="white-text"><i className="fa fa-facebook-official"></i> PPSR 2K15</a></li>
            <li className="white-text"><a href="https://www.instagram.com/ppsr_official/" className="white-text"><i className="fa fa-instagram"></i> ppsr_official</a></li>
          </ul>
        </Col>
      </Row>
    </div>
    <div className="footer-copyright">
      <div className="container">
        PPSR &copy; <span className="blue-text text-lighten-3">2018</span>
      </div>
    </div>
  </footer>
);

export default footer;