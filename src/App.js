import React, { Component } from 'react';
import { Route, Switch } from "react-router-dom";

// import Components
import Home from './containers/Home/Home';
import EventPage from './containers/EventPage/EventPage';
import RegisterEvent from './containers/RegisterEvent/RegisterEvent';
import SuccessRegister from './components/SuccessRegister/SuccessRegister';
import Ticket from './containers/Ticket/Ticket';
import TestTable from './containers/TestTable/TestTable';
import Navbar from './containers/Navbar/Navbar';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        <Switch>
          <Route path="/test-table" component={TestTable} />
          <Route path="/events/:id/ticket" component={Ticket} />
          <Route path="/events/:eventId/register/success" component={SuccessRegister} />
          <Route path="/events/:eventId/register" component={RegisterEvent} />
          <Route path="/events" component={EventPage} />
          <Route path="/" component={Home} />
        </Switch>
      </div>
    );
  }
}

export default App;
